<?php

Route::group([
    'prefix' => config('saml2_settings.routesPrefix'),
    'middleware' => config('saml2_settings.routesMiddleware'),
], function () {

    Route::get('/logout', array(
        'as' => 'saml_logout',
        'uses' => 'Singh\SimpleSaml\Controllers\SimpleSamlController@logout',
    ));

    Route::get('/login', array(
        'as' => 'saml_login',
        'uses' => 'Singh\SimpleSaml\Controllers\SimpleSamlController@login',
    ));

    Route::post('/acs', array(
        'as' => 'saml_acs',
        'uses' => 'Singh\SimpleSaml\Controllers\SimpleSamlController@acs',
    ));

    /**
     * Use the below routes if needed.
     *
    Route::get('/metadata', array(
        'as' => 'saml_metadata',
        'uses' => 'Singh\SimpleSaml\Controllers\SimpleSamlController@metadata',
    ));

    Route::get('/sls', array(
        'as' => 'saml_sls',
        'uses' => 'Singh\SimpleSaml\Controllers\SimpleSamlController@sls',
    ));
    */
});
